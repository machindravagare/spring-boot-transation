package ai.wavelabs.demo.module;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GeneratorType;
import org.springframework.web.bind.annotation.GetMapping;

@Entity
@Table(name = "employee143")
public class EmployeeCache {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private int Empid;
	@Column
	private String name;
	@Column
	private String address;
	@Column
	private int mobileNo;

	


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(int mobileNo) {
		this.mobileNo = mobileNo;
	}

	public int getEmpid() {
		return Empid;
	}

	public void setEmpid(int empid) {
		Empid = empid;
	}


	

}
