package ai.wavelabs.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TransactionCacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionCacheApplication.class, args);
	}

}
