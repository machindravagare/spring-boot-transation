package ai.wavelabs.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.wavelabs.demo.module.EmployeeCache;

public interface EmployeeCacheRepository extends JpaRepository<EmployeeCache, Integer> {

}
