package ai.wavelabs.demo.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import ai.wavelabs.demo.module.EmployeeCache;
import ai.wavelabs.demo.repository.EmployeeCacheRepository;

@Service
public class EmployeeCacheService {
	
	@Autowired
	public EmployeeCacheRepository emprepository;
	
	@org.springframework.cache.annotation.Cacheable(cacheNames = "person")
	public List<EmployeeCache> getAllEmployeeCache() 
	{
		List<EmployeeCache> li=emprepository.findAll();
		return li;
		
	}
	
	public EmployeeCache saveEmployeeCache(EmployeeCache cacheee) {
		return emprepository.save(cacheee);
	}

	
	@CachePut(cacheNames = "students", key = "#po.empid")
	public EmployeeCache updateEmployeeCache(EmployeeCache cacheee, Integer id) {
		cacheee.setEmpid(id);
		EmployeeCache student2 = emprepository.saveAndFlush(cacheee);
		return student2;
	}

	@CacheEvict(key = "#id", cacheNames = "students")
	public void deleteEmployeeCache(Integer id) {
		emprepository.deleteById(id);
	}
}
