package ai.wavelabs.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ai.wavelabs.demo.module.EmployeeCache;
import ai.wavelabs.demo.service.EmployeeCacheService;

@RestController
public class EmployeeController {
	@Autowired
	public EmployeeCacheService employeeservice;

	@GetMapping(value = "employees")
	public ResponseEntity<List<EmployeeCache>> getEmployeeCache() {

		List<EmployeeCache> employeee = employeeservice.getAllEmployeeCache();
		return ResponseEntity.status(200).body(employeee);
	}

	@PostMapping(value = "employees")
	public ResponseEntity<EmployeeCache> updateEmployeeCache(@RequestBody EmployeeCache cacheemp) {
		return ResponseEntity.status(201).body(employeeservice.saveEmployeeCache(cacheemp));
	}

}
